package com.id.domain.auth

import com.id.domain.NetworkResponse
import com.id.domain.auth.model.AuthDataModel
import kotlinx.coroutines.flow.Flow

interface IAuthRepository {
    suspend fun login(email: String, password: String): NetworkResponse<AuthDataModel>
    suspend fun register(name: String, email: String, password: String): NetworkResponse<AuthDataModel>
}