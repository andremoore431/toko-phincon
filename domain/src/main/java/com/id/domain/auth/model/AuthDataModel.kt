package com.id.domain.auth.model

data class AuthDataModel(
    val accessToken: String,
    val expiresAt: Int,
    val refreshToken: String,
    val profileName: String,
    val profileUrl: String
)
