package com.id.domain.auth

import com.id.domain.NetworkResponse
import com.id.domain.Resource

class RegisterUseCase(
    private val authRepository: IAuthRepository
) {
    suspend operator fun invoke(name: String, email: String, password: String): Resource<Boolean> {
        return when (val response = authRepository.register(name = name, email = email, password = password)) {
            NetworkResponse.Empty -> {
                Resource.Error("Empty Data")
            }
            is NetworkResponse.Error -> {
                Resource.Error(response.error)
            }
            is NetworkResponse.Success -> {
                Resource.Success(true)
            }
        }
    }
}