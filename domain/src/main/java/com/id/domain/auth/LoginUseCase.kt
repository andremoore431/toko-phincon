package com.id.domain.auth

import com.id.domain.NetworkResponse
import com.id.domain.Resource
import com.id.domain.session.ISessionRepository

class LoginUseCase(
    private val authRepository: IAuthRepository,
    private val sessionRepository: ISessionRepository
) {
    suspend operator fun invoke(email: String, password: String): Resource<Boolean> {
        return when (val response = authRepository.login(email = email, password = password)) {
            NetworkResponse.Empty -> {
                Resource.Error("Empty Data")
            }
            is NetworkResponse.Error -> {
                Resource.Error(response.error)
            }
            is NetworkResponse.Success -> {
                sessionRepository.setProfileUrl(response.data.profileUrl)
                sessionRepository.setProfileName(response.data.profileName)
                Resource.Success(true)
            }
        }
    }
}