package com.id.domain

sealed class NetworkResponse<out T> {
    data object Empty: NetworkResponse<Nothing>()
    data class Success<T>(val data: T): NetworkResponse<T>()
    data class Error(val error: String): NetworkResponse<Nothing>()
}