package com.id.domain.profile

import com.id.domain.NetworkResponse
import com.id.domain.Resource

interface IProfileRepository {
    suspend fun updateProfile(userName: String, profileImage: ByteArray): NetworkResponse<ProfileDataModel>
}