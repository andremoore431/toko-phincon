package com.id.domain.profile

data class ProfileDataModel(
    val userName: String,
    val userProfileUrl: String,
)
