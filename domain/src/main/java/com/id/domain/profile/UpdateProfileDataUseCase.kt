package com.id.domain.profile

import com.id.domain.NetworkResponse
import com.id.domain.Resource
import com.id.domain.session.ISessionRepository

class UpdateProfileDataUseCase(
    private val profileRepository: IProfileRepository,
    private val sessionRepository: ISessionRepository
) {
    suspend operator fun invoke(userName: String, userProfile: ByteArray): Resource<Boolean> {
        val response = profileRepository.updateProfile(
            userName = userName,
            profileImage = userProfile
        )
        return when (response) {
            NetworkResponse.Empty -> {
                Resource.Error("Empty Data")
            }

            is NetworkResponse.Error -> {
                Resource.Error(response.error)
            }

            is NetworkResponse.Success -> {
                response.data.let {
                    sessionRepository.setProfileName(it.userName)
                    sessionRepository.setProfileUrl(it.userProfileUrl)
                }
                Resource.Success(true)
            }
        }
    }
}