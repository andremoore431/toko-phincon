package com.id.domain.di

import com.id.domain.auth.LoginUseCase
import com.id.domain.auth.RegisterUseCase
import com.id.domain.profile.UpdateProfileDataUseCase
import com.id.domain.session.LoginStatusUseCase
import com.id.domain.session.UserStatusUseCase
import org.koin.dsl.module

object DomainModule {
    private val useCaseModule = module {
        single { LoginUseCase(get(), get()) }
        single { RegisterUseCase(get()) }
        single { LoginUseCase(get(), get()) }
        single { LoginStatusUseCase(get()) }
        single { UserStatusUseCase(get()) }
        single { UpdateProfileDataUseCase(get(), get()) }
    }

    fun getModules() = listOf(
        useCaseModule
    )
}