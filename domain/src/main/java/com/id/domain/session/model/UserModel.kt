package com.id.domain.session.model

data class UserModel(
    val userName: String,
    val profileURL: String
)
