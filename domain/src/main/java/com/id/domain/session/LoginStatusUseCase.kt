package com.id.domain.session

class LoginStatusUseCase(
    private val sessionRepository: ISessionRepository
) {
    operator fun invoke() = sessionRepository.isUserLogin
}