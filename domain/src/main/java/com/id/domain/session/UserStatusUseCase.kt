package com.id.domain.session

class UserStatusUseCase (
    private val sessionRepository: ISessionRepository
) {
    operator fun invoke() = sessionRepository.userProfile
}