package com.id.domain.session

import com.id.domain.session.model.UserModel
import kotlinx.coroutines.flow.Flow

interface ISessionRepository {
    val isUserLogin: Flow<Boolean?>
    val userProfile: Flow<UserModel>
    suspend fun setProfileUrl(url: String)
    suspend fun setProfileName(name: String)
}