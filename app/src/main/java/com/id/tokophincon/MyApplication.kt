package com.id.tokophincon

import android.app.Application
import com.id.data.di.DataModule
import com.id.domain.di.DomainModule
import com.id.tokophincon.di.AppModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(DataModule.getModules())
            modules(DomainModule.getModules())
            modules(AppModule.getModules())
        }
    }
}