package com.id.tokophincon.ui.auth.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.id.domain.Resource
import com.id.domain.auth.LoginUseCase
import com.id.tokophincon.util.UIState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class LogInViewModel constructor(
    private val loginUseCase: LoginUseCase
) : ViewModel() {
    private val _uiState = MutableStateFlow<UIState<Boolean>?>(null)
    val uiState = _uiState.asStateFlow()

    fun login(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _uiState.emit(UIState.Loading)
            val response = loginUseCase(email, password)
            _uiState.emit(
                when (response) {
                    is Resource.Error -> {
                        UIState.Error(response.error)
                    }
                    is Resource.Success -> {
                        UIState.Success(true)
                    }
                }
            )
        }
    }
}