package com.id.tokophincon.ui.profile

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import coil.load
import com.id.tokophincon.R
import com.id.tokophincon.base.BaseFragment
import com.id.tokophincon.databinding.FragmentProfileBinding
import com.id.tokophincon.ui.camera.CameraActivity
import com.id.tokophincon.ui.camera.CameraActivity.Companion.CAMERAX_RESULT
import com.id.tokophincon.ui.camera.CameraActivity.Companion.CAMERA_X_IMAGE_RESULT
import com.id.tokophincon.util.MediaHelper.uriToByteArray
import com.id.tokophincon.util.UIState
import com.id.tokophincon.util.gone
import com.id.tokophincon.util.launchAndCollectIn
import com.id.tokophincon.util.visible
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.fragmentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.scope.Scope

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>(
    FragmentProfileBinding::inflate
), AndroidScopeComponent {
    override val viewModel: ProfileViewModel by viewModel()
    private var imageUri: Uri = Uri.EMPTY


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initListener()
    }

    private val pickMedia =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            if (uri != null) {
                onUriSetUp(uri)
            }
        }

    private val cameraCapture: ActivityResultLauncher<Intent> = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == CAMERAX_RESULT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                val uriResult =
                    it.data?.getSerializableExtra(CAMERA_X_IMAGE_RESULT, String::class.java)
                uriResult?.let { imageResult ->
                    imageUri = imageResult.toUri()
                    onUriSetUp(imageUri)
                }
            }
        }
    }

    override fun initListener() {
        with(binding) {
            fpIvProfile.setOnClickListener {
                showImageDialog()
            }

            fpBtDone.setOnClickListener {
                uploadNameAndImage()
            }

            viewModel.uiState.launchAndCollectIn(viewLifecycleOwner) {
                when (it) {
                    is UIState.Error -> {
                        fpProgressBar.gone()
                        showMessage(it.errorMessage)
                    }

                    UIState.Loading -> {
                        fpProgressBar.visible()
                    }

                    is UIState.Success -> {
                        fpProgressBar.gone()
                        showMessage(it.data.toString())
                    }

                    else -> {}
                }
            }
        }
    }

    private fun onUriSetUp(uri: Uri) {
        imageUri = uri
        binding.fpIvProfile.load(
            uri
        )
    }

    private fun uploadNameAndImage() {
        with(binding) {
            val name = fpEtName.text.toString()
            if (name.isEmpty() || imageUri == Uri.EMPTY) {
                showMessage("Please Fill Your Name and Choose Image")
                return
            }
            val imageByteArray = uriToByteArray(requireContext(), imageUri)
            if (imageByteArray == null) {
                showMessage("Image Is Not Valid")
                return
            }
            viewModel.uploadProfileData(userName = name, userProfile = imageByteArray)
        }
    }

    private fun showImageDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder
            .setTitle(getString(R.string.text_choose_image))
            .setItems(this.resources.getStringArray(R.array.image_picker)) { _, which ->
                when (which) {
                    0 -> {
                        val intent = Intent(requireActivity(), CameraActivity::class.java)
                        cameraCapture.launch(intent)
                    }

                    1 -> {
                        pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
                    }
                }
            }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override val scope: Scope by fragmentScope()
}