package com.id.tokophincon.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.id.domain.Resource
import com.id.domain.profile.UpdateProfileDataUseCase
import com.id.tokophincon.util.UIState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val updateProfileDataUseCase: UpdateProfileDataUseCase
) : ViewModel() {
    private val _uiState = MutableStateFlow<UIState<Boolean>?>(null)
    val uiState = _uiState.asStateFlow()

    fun uploadProfileData(
        userName: String,
        userProfile: ByteArray
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            _uiState.emit(UIState.Loading)
            val result = updateProfileDataUseCase(userName = userName, userProfile = userProfile)
            when (result) {
                is Resource.Error -> {
                    _uiState.emit(UIState.Error(result.error))
                }

                is Resource.Success -> {
                    _uiState.emit(UIState.Success(true))
                }
            }
        }
    }
}