package com.id.tokophincon.ui.auth.register

import android.content.Intent
import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.navigation.fragment.findNavController
import com.id.tokophincon.R
import com.id.tokophincon.base.BaseFragment
import com.id.tokophincon.databinding.FragmentRegisterBinding
import com.id.tokophincon.util.UIState
import com.id.tokophincon.util.Utils.privacyDummyURL
import com.id.tokophincon.util.Utils.termsDummyURL
import com.id.tokophincon.util.gone
import com.id.tokophincon.util.launchAndCollectIn
import com.id.tokophincon.util.visible
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.fragmentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.scope.Scope

class RegisterFragment : BaseFragment<FragmentRegisterBinding, RegisterViewModel>(
    FragmentRegisterBinding::inflate
), AndroidScopeComponent {
    override val viewModel: RegisterViewModel by viewModel()
    private lateinit var spannableString: SpannableString

    override fun initView() {
        val agreementText = getString(R.string.agreement_text)
        spannableString = SpannableString(agreementText)
    }

    override fun initListener() {
        with(binding) {

            setSpannableText()

            registerButton.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()

                viewModel.register("", email, password)
            }

            loginButton.setOnClickListener {
                navigateToLogin()
            }
        }
    }

    override fun listenData() {
        with(binding) {
            viewModel.uiState.launchAndCollectIn(owner = viewLifecycleOwner) {
                when (it) {
                    is UIState.Error -> {
                        showMessage(it.errorMessage)
                        registerLoadingBar.gone()
                    }
                    UIState.Loading -> {
                        registerLoadingBar.visible()
                    }
                    is UIState.Success -> {
                        showMessage("Success")
                        registerLoadingBar.gone()
                        navigateToProfile()
                    }
                    else -> {}
                }
            }
        }
    }

    override val scope: Scope by fragmentScope()

    private fun navigateToProfile() {
        val action = RegisterFragmentDirections.actionRegisterFragment2ToProfileFragment()
        findNavController().navigate(action)
    }

    private fun setSpannableText() {
        with(binding) {
            val termsText = getString(R.string.terms_and_conditions)
            val privacyText = getString(R.string.privacy_policy)

            val termsClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(termsDummyURL))
                    startActivity(intent)
                }
            }

            val privacyClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(privacyDummyURL))
                    startActivity(intent)
                }
            }

            val termsStart = spannableString.indexOf(termsText)
            val termsEnd = termsStart + termsText.length
            spannableString.setSpan(termsClickableSpan, termsStart, termsEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

            val privacyStart = spannableString.indexOf(privacyText)
            val privacyEnd = privacyStart + privacyText.length
            spannableString.setSpan(privacyClickableSpan, privacyStart, privacyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

            // Set the SpannableString on the TextView
            tvTermsConditions.text = spannableString
            tvTermsConditions.movementMethod = LinkMovementMethod.getInstance() // Make the links clickable

        }
    }

    private fun navigateToLogin() {
        val action = RegisterFragmentDirections.actionRegisterFragment2ToLogInFragment2()
        findNavController().navigate(action)
    }
}