package com.id.tokophincon

import androidx.lifecycle.ViewModel
import com.id.domain.auth.LoginUseCase
import com.id.domain.session.LoginStatusUseCase
import com.id.domain.session.UserStatusUseCase

class MainViewModel constructor(
    private val statusUseCase: LoginStatusUseCase,
    private val userStatusUseCase: UserStatusUseCase
): ViewModel() {
    val isLogin = statusUseCase()
    val userStatus = userStatusUseCase()
}