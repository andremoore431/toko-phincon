package com.id.tokophincon.util

sealed class UIState<out T> {
    data object Loading: UIState<Nothing>()
    data class Error(val errorMessage: String) : UIState<Nothing>()
    data class Success<T>(val data: T) : UIState<T>()
}