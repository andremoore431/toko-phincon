package com.id.tokophincon.di

import com.id.tokophincon.MainActivity
import com.id.tokophincon.MainViewModel
import com.id.tokophincon.ui.auth.login.LogInFragment
import com.id.tokophincon.ui.auth.login.LogInViewModel
import com.id.tokophincon.ui.auth.register.RegisterFragment
import com.id.tokophincon.ui.auth.register.RegisterViewModel
import com.id.tokophincon.ui.profile.ProfileFragment
import com.id.tokophincon.ui.profile.ProfileViewModel
import org.koin.dsl.module

object AppModule {
    private val viewModelModule = module {
        scope<LogInFragment> { scoped { LogInViewModel(get()) } }
        scope<RegisterFragment> { scoped { RegisterViewModel(get()) } }
        scope<MainActivity> {scoped { MainViewModel(get(), get()) }}
        scope<ProfileFragment> { scoped { ProfileViewModel(get()) } }
    }

    fun getModules() = listOf(
        viewModelModule
    )
}