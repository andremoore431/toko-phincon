package com.id.tokophincon

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.coroutineScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.id.tokophincon.databinding.ActivityMainBinding
import com.id.tokophincon.ui.auth.login.LogInFragmentDirections
import com.id.tokophincon.ui.auth.register.RegisterFragmentDirections
import com.id.tokophincon.ui.home.MainHomeFragmentDirections
import com.id.tokophincon.ui.profile.ProfileFragmentDirections
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.activityScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.scope.Scope

class MainActivity : AppCompatActivity(), AndroidScopeComponent {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModel()
    private lateinit var mainNavController: NavController

    private val requiredPermissions = arrayOf(
        Manifest.permission.CAMERA,
    )

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        val deniedPermissions = permissions.filterValues { !it }.keys
        if (deniedPermissions.isNotEmpty()) {
            Toast.makeText(this, "Permissions denied: $deniedPermissions", Toast.LENGTH_SHORT)
                .show()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        if (!arePermissionsGranted()) {
            requestPermissions()
        }

        val mainNavHostFragment =
            supportFragmentManager.findFragmentById(R.id.mainFragmentContainer) as NavHostFragment
        mainNavController = mainNavHostFragment.navController


        lifecycle.coroutineScope.launch {
            viewModel.isLogin.collect { loginStatus ->
                if (loginStatus == true) {
                    val userStatus = viewModel.userStatus.first()
                    if (userStatus.userName.isNotEmpty() || userStatus.profileURL.isNotEmpty()) {
                        navigateToHome()
                    } else {
                        navigateToProfile()
                    }
                } else if (loginStatus == false) {
                    navigateToLogin()
                }
            }
        }
    }

    private fun navigateToLogin() {
        val action = MainHomeFragmentDirections.actionMainHomeFragmentToLogInFragment()
        mainNavController.navigate(action)
    }

    private fun navigateToProfile() {
        val currentDestination = mainNavController.currentDestination?.id
        when (currentDestination) {
            R.id.registerFragment -> {
                val action = RegisterFragmentDirections.actionRegisterFragment2ToProfileFragment()
                mainNavController.navigate(action)
            }

            R.id.logInFragment -> {
                val action = LogInFragmentDirections.actionLogInFragment2ToProfileFragment()
                mainNavController.navigate(action)
            }
        }

    }

    private fun navigateToHome() {
        val currentDestination = mainNavController.currentDestination?.id
        when (currentDestination) {
            R.id.profileFragment -> {
                val action = ProfileFragmentDirections.actionProfileFragmentToMainHomeFragment()
                mainNavController.navigate(action)
            }

            R.id.logInFragment -> {
                val action = LogInFragmentDirections.actionLogInFragmentToMainHomeFragment()
                mainNavController.navigate(action)
            }
        }

    }

    override val scope: Scope by activityScope()

    private fun arePermissionsGranted(): Boolean {
        for (permission in requiredPermissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    private fun requestPermissions() {
        requestPermissionLauncher.launch(requiredPermissions)
    }
}
