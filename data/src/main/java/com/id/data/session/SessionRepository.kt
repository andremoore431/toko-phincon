package com.id.data.session

import android.util.Log
import com.id.data.session.source.ISessionSource
import com.id.domain.session.ISessionRepository
import com.id.domain.session.model.UserModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class SessionRepository(
    private val sessionSource: ISessionSource
): ISessionRepository {
    override val isUserLogin: Flow<Boolean?> = sessionSource.isLogin()
    override val userProfile: Flow<UserModel> = flow {
        emit(UserModel(
            userName = sessionSource.getProfileName().first(),
            profileURL = sessionSource.getProfileName().first()
        ))
    }

    override suspend fun setProfileUrl(url: String) {
        sessionSource.setProfileUrl(url)
    }

    override suspend fun setProfileName(name: String) {
        sessionSource.setProfileName(name)
    }
}