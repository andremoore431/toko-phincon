package com.id.data.session.source

import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.flow.Flow

interface ISessionSource {
    val jwtToken: Preferences.Key<String>
    val refreshToken: Preferences.Key<String>
    val profileImageUrl: Preferences.Key<String>
    val profileName: Preferences.Key<String>

    fun isLogin(): Flow<Boolean?>
    fun getToken(): Flow<String>
    fun getRefreshToken(): Flow<String>
    fun getProfileName(): Flow<String>
    fun getProfileImage(): Flow<String>

    suspend fun setUserToken(token: String)
    suspend fun setRefreshToken(token: String)
    suspend fun clearSession()
    suspend fun setProfileUrl(url: String)
    suspend fun setProfileName(name: String)
}