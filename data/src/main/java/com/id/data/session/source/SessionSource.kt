package com.id.data.session.source

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class SessionSource(
    private val dataStore: DataStore<Preferences>
): ISessionSource {
    override val jwtToken = stringPreferencesKey(JWT_TOKEN)
    override val refreshToken = stringPreferencesKey(REFRESH_TOKEN)
    override val profileImageUrl: Preferences.Key<String> = stringPreferencesKey(IMAGE_PROFILE_URL)
    override val profileName: Preferences.Key<String> = stringPreferencesKey(NAME_PROFILE)

    override fun isLogin() = dataStore.data.map {
        it[jwtToken]?.isNotEmpty()
    }

    override fun getToken(): Flow<String> = dataStore.data.map {
        it[jwtToken] ?: ""
    }

    override fun getRefreshToken() = dataStore.data.map {
        it[refreshToken] ?: ""
    }

    override fun getProfileName(): Flow<String> = dataStore.data.map {
        it[profileName] ?: ""
    }

    override fun getProfileImage(): Flow<String> = dataStore.data.map {
        it[profileImageUrl] ?: ""
    }

    override suspend fun setUserToken(token: String) {
        dataStore.edit {
            it[jwtToken] = token
        }
    }

    override suspend fun setRefreshToken(token: String) {
        dataStore.edit {
            it[refreshToken] = token
        }
    }

    override suspend fun clearSession() {
        dataStore.edit {
            it.clear()
        }
    }

    override suspend fun setProfileUrl(url: String) {
        dataStore.edit {
            it[profileImageUrl] = url
        }
    }

    override suspend fun setProfileName(name: String) {
        dataStore.edit {
            it[profileName] = name
        }
    }

    companion object {
        private const val JWT_TOKEN = "jwtToken"
        private const val REFRESH_TOKEN = "refreshToken"
        private const val IMAGE_PROFILE_URL = "imageProfileURL"
        private const val NAME_PROFILE = "nameProfile"
    }
}