package com.id.data.auth.model.request

data class RefreshRequest(
    val token: String
)
