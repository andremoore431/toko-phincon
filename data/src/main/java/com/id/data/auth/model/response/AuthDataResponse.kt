package com.id.data.auth.model.response

data class AuthDataResponse(
	val accessToken: String? = null,
	val expiresAt: Int? = null,
	val refreshToken: String? = null,
	val userName: String? = null,
	val userImage: String? = null
)