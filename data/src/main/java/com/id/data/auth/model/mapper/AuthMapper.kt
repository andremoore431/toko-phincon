package com.id.data.auth.model.mapper

import com.id.data.auth.model.response.AuthDataResponse
import com.id.domain.auth.model.AuthDataModel

object AuthMapper {
    fun mapAuthResponseToModel(q: AuthDataResponse) = AuthDataModel(
        accessToken = q.accessToken ?: "", expiresAt = q.expiresAt ?: 0, refreshToken = q.refreshToken ?: "",
        profileName = q.userName ?: "",
        profileUrl = q.userImage ?: ""
    )
}