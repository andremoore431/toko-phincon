package com.id.data.auth

import android.util.Log
import com.id.data.auth.model.mapper.AuthMapper.mapAuthResponseToModel
import com.id.data.auth.model.request.LoginRequest
import com.id.data.auth.model.request.RegisterRequest
import com.id.data.auth.source.AuthApiService
import com.id.data.session.source.ISessionSource
import com.id.domain.NetworkResponse
import com.id.domain.auth.model.AuthDataModel
import com.id.domain.auth.IAuthRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class AuthRepository constructor(
    private val authApiService: AuthApiService,
    private val sessionSource: ISessionSource
): IAuthRepository {
    override suspend fun login(email: String, password: String): NetworkResponse<AuthDataModel> {
        val request = LoginRequest(email, password, "")
        return try {
            val response = authApiService.login(request)
            if (response.data != null) {
                sessionSource.setUserToken(response.data.accessToken ?: "")
                sessionSource.setRefreshToken(response.data.refreshToken ?: "")
                NetworkResponse.Success(mapAuthResponseToModel(response.data))
            } else {
                NetworkResponse.Empty
            }

        } catch (e: Exception) {
            NetworkResponse.Error(e.message.toString())
        }
    }

    override suspend fun register(name: String, email: String, password: String): NetworkResponse<AuthDataModel> {
        val request = RegisterRequest(email, password, "")
        return try {
            val response = authApiService.register(request)
            if (response.data != null) {
                sessionSource.setUserToken(response.data.accessToken ?: "")
                sessionSource.setRefreshToken(response.data.refreshToken ?: "")
                NetworkResponse.Success(mapAuthResponseToModel(response.data))
            } else {
                NetworkResponse.Empty
            }
        } catch (e: Exception) {
            NetworkResponse.Error(e.message.toString())
        }
    }
}