package com.id.data.auth.source

import com.id.data.auth.model.request.LoginRequest
import com.id.data.auth.model.request.RefreshRequest
import com.id.data.auth.model.request.RegisterRequest
import com.id.data.auth.model.response.AuthDataResponse
import com.id.data.auth.model.response.AuthResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthApiService {
    @POST("login")
    suspend fun login(
        @Body body: LoginRequest
    ): AuthResponse

    @POST("register")
    suspend fun register(
        @Body body: RegisterRequest
    ): AuthResponse

    @POST("refresh")
    suspend fun refresh(
        @Body body: RefreshRequest
    ): AuthResponse
}