package com.id.data.network.interceptor

import com.id.data.BuildConfig
import com.id.data.session.source.ISessionSource
import com.id.data.utils.PathSegment
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(
    private val session: ISessionSource
): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val path = chain.request().url.pathSegments.first()
        val authPathList = PathSegment::class.sealedSubclasses.mapNotNull { it.objectInstance?.path }

        val requestBuilder = chain.request().newBuilder()
        requestBuilder.addHeader("Content-Type", "application/json")

        if (authPathList.contains(path)) {
            requestBuilder.addHeader("API_KEY", BuildConfig.API_KEY)
            return chain.proceed(requestBuilder.build())
        }

        val token = runBlocking {
            session.getToken().first()
        }
        requestBuilder.addHeader("Authorization", "Bearer $token")
        return chain.proceed(requestBuilder.build())
    }
}