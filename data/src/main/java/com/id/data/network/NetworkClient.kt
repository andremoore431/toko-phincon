package com.id.data.network

import com.id.data.BuildConfig
import com.id.data.network.authenticator.UserAuthenticator
import com.id.data.network.interceptor.AuthInterceptor
import com.id.data.network.interceptor.SessionInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit
import kotlin.math.log

class NetworkClient(
    val loggingInterceptor: HttpLoggingInterceptor,
    val sessionInterceptor: SessionInterceptor,
    val authInterceptor: AuthInterceptor,
    val userAuthenticator: UserAuthenticator
) {
    inline fun <reified I> create(): I {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(sessionInterceptor)
            .addInterceptor(authInterceptor)
            .addInterceptor(loggingInterceptor)
            .authenticator(userAuthenticator)
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(I::class.java)
    }
}