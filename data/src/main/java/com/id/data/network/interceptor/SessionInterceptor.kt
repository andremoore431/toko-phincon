package com.id.data.network.interceptor

import android.util.Log
import com.id.data.BuildConfig
import com.id.data.session.source.ISessionSource
import com.id.data.utils.PathSegment
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response

class SessionInterceptor(
    private val session: ISessionSource
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        if (response.code == 401) {
            runBlocking {
                session.clearSession()
            }
        }
        return response
    }
}

