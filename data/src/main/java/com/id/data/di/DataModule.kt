package com.id.data.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.id.data.auth.AuthRepository
import com.id.data.auth.source.AuthApiService
import com.id.data.network.NetworkClient
import com.id.data.network.authenticator.UserAuthenticator
import com.id.data.network.interceptor.AuthInterceptor
import com.id.data.network.interceptor.SessionInterceptor
import com.id.data.profile.ProfileRepository
import com.id.data.profile.source.ProfileApiService
import com.id.data.session.SessionRepository
import com.id.data.session.source.ISessionSource
import com.id.data.session.source.SessionSource
import com.id.domain.auth.IAuthRepository
import com.id.domain.profile.IProfileRepository
import com.id.domain.session.ISessionRepository
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "userDataStore")

object DataModule {
    private val interceptor = module {
        single { HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY) }
        single { SessionInterceptor(get()) }
        single { AuthInterceptor(get()) }
        single { UserAuthenticator(get(), get()) }
    }

    private val networkModule = module {
        single { NetworkClient(get(), get(), get(), get()) }
        single<AuthApiService> { get<NetworkClient>().create() }
        single<ProfileApiService> {get<NetworkClient>().create()}
    }

    private val dataStoreModule = module {
        single<DataStore<Preferences>> {androidContext().dataStore}
        single<ISessionSource> { SessionSource(get()) }
    }


    private val repositoryModule = module {
        single<IAuthRepository> { AuthRepository(get(), get()) }
        single<ISessionRepository> {SessionRepository(get())}
        single<IProfileRepository> {ProfileRepository(get())}
    }

    fun getModules() = listOf(
        interceptor,
        networkModule,
        dataStoreModule,
        repositoryModule
    )
}