package com.id.data.profile.model.response

import com.google.gson.annotations.SerializedName

data class UpdateProfileResponse(

    @field:SerializedName("code")
	val code: Int? = null,

    @field:SerializedName("data")
	val data: UpdateProfileDataResponse? = null,

    @field:SerializedName("message")
	val message: String? = null
)

