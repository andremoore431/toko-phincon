package com.id.data.profile

import com.id.data.profile.model.mapper.ProfileMapper.profileResponseToModel
import com.id.data.profile.source.ProfileApiService
import com.id.domain.NetworkResponse
import com.id.domain.Resource
import com.id.domain.profile.IProfileRepository
import com.id.domain.profile.ProfileDataModel
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class ProfileRepository(
    private val profileApiService: ProfileApiService
): IProfileRepository {
    override suspend fun updateProfile(
        userName: String,
        profileImage: ByteArray
    ): NetworkResponse<ProfileDataModel> {
        return try {
            val userNameBody = userName.toRequestBody("text/plain".toMediaTypeOrNull())
            val imageBody = profileImage.toRequestBody("image/*".toMediaTypeOrNull())
            val userImagePart = MultipartBody.Part.createFormData("userImage", "User Profile Image", imageBody)
            val response = profileApiService.updateProfile(userNameBody, userImagePart)
            if (response.data != null) {
                NetworkResponse.Success(profileResponseToModel(response.data))
            } else {
                NetworkResponse.Empty
            }
        } catch (e: Exception) {
            NetworkResponse.Error(e.message.toString())
        }
    }
}