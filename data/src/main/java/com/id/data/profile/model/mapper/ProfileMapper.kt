package com.id.data.profile.model.mapper

import com.id.data.profile.model.response.UpdateProfileDataResponse
import com.id.domain.profile.ProfileDataModel

object ProfileMapper {
    fun profileResponseToModel(q: UpdateProfileDataResponse) = ProfileDataModel(
        userName = q.userName ?: "", userProfileUrl = q.userImage ?: ""
    )
}