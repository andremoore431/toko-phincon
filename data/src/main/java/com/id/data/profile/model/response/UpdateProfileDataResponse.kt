package com.id.data.profile.model.response

import com.google.gson.annotations.SerializedName

data class UpdateProfileDataResponse(

	@field:SerializedName("userImage")
	val userImage: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null
)