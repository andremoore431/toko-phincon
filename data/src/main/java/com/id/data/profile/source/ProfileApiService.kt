package com.id.data.profile.source

import com.id.data.profile.model.response.UpdateProfileResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ProfileApiService {
    @Multipart
    @POST("profile")
    suspend fun updateProfile(
        @Part("userName") userName: RequestBody,
        @Part userImage: MultipartBody.Part
    ): UpdateProfileResponse
}