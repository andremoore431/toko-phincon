package com.id.data.utils

object Utils {
    const val LOGIN = "login"
    const val REGISTER = "register"
    const val REFRESH = "refresh"
}