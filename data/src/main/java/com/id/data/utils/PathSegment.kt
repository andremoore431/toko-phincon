package com.id.data.utils

sealed class PathSegment(val path: String) {
    data object Login : PathSegment("login")
    data object REGISTER : PathSegment("register")
    data object REFRESH : PathSegment("refresh")
}